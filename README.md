## [@Webkit.ipfs/GPG](npmjs.com/package/@webkit.ipfs/gpg)


 * Note: `webkit.ipfs/gpg` built using synchronous versions of Node utilities:
   - fs.existsSync()
   - child_process.execSync()
   - etc.

 * Our plan is to reimpliment as asynchronous library, as the demands of the Webkit.ipfs project require.

### Usage
```js
const GPG = require('@webkit.ipfs/gpg')
const Path = require('path')
const HOME = process.env.HOME

let alice = 'alice@my.org'
let bob = 'bob@my.org'

//* Does the key exist?
GPG.keysExist(alice) // => false
GPG.keysExist(bob) // => false

//* Create keys:
let path = Path.join(HOME, '.gpg')
GPG.generateKeys(path, alice, 'A_SecretPassphrase')
GPG.generateKeys(path, bob, 'B_SecretPassphrase')

//* Authenticate key:
GPG.authenticate(alice, 'BadGuess') // => false
GPG.authenticate(alice, 'A_SecretPassphrase') // => true

//* Encrypt secrets:
let secrets = fs.readFileSync('secrets.txt')
let encrypted = GPG.encrypt(secrets, alice)
GPG.decrypt(encrypted, 'A_SecretPassphrase') // => "Alice's secrets"

//* Encrypt message for recipient:
let bobsMessage = GPG.encrypt("Hey Bob, it's Alice", bob)
GPG.decrypt(bobsMessage, 'B_SecretPassphrase') // => "Hey Bob, it's Alice"
```
