const sh = require('child_process')

module.exports = {
   keysExist: (gpg_id) => {
      if (!gpg_id) return false
      try {
         sh.execSync(`gpg --list-key ${gpg_id} > /dev/null 2>&1`)
         return true
      }
      catch (err) {
         return false
      }  
   },

   authenticate: (gpg_id, pw) => {
      try {
         sh.execSync(`gpg --pinentry-mode loopback --passphrase=${pw} -o /dev/null -a --export-secret-key ${gpg_id}`, {stdio: 'ignore'})
         return true
      }
      catch (error) {
         return false
      }
   },

   generateKeys (path, gpg_id, pw='') {  
      let opts = {
         input: pw,
         stdio: 'ignore',
         cwd: path
      }
      sh.execSync(`gpg --batch --passphrase-fd 0 --quick-gen-key ${gpg_id} > /dev/null 2>&1`, opts)
      //? let fingerprint = sh.execSync(`gpg --list-key ${nid}`).toString().split('\n')[1].trim()
      //? sh.execSync(`gpg --batch --passphrase-fd 0 --quick-set-expire ${fingerprint} never > /dev/null 2>&1`, opts)
      sh.execSync(`gpg --batch --passphrase-fd 0 --output public.key --armor --export ${gpg_id} > /dev/null 2>&1`, opts)
      return GPG.keysExist(key)
   },

   ultimatelyTrusted: (nid) => {
      if (!GPG.keysExist(nid)) return false
      let key_details = sh.execSync(`echo $(gpg --list-key ${nid}) | grep ' ${nid} '`).toString()
      return Boolean(key_details.search(/\[ultimate\]/) !== -1)
   },

   //! Required for importing keys:
   /* trustUltimately: ({ nid, pw }) => {
      log(`Feature not implimented yet.`)
   }, */

   encrypt: (input, gpg_id, pw=null) => {
      if (!input) throw "GPG requires data." 
      if (!gpg_id) throw "GPG requires gpg_id.\nFor passphrase-only encryption, use: `PGP.encrypt(data, { passwords:['secret'] })`"
      
      const pass = () => `--pinentry-mode loopback --passphrase=${pw} `
      let command = (pw)
         ? `gpg ${pass()}-esr ${gpg_id} -o-`
         : `gpg --encrypt -sr ${gpg_id} -o-`

      return sh.execSync(command, { input })
   },

   decrypt: (input, pw=null) => {

      if (!input) throw "GPG requires data." 

      const pass = () => `--pinentry-mode loopback --passphrase=${pw} `
      let command = (pw)
         ? `gpg ${pass()}-d -o- 2>/dev/null`
         : `gpg --decrypt -o- 2>/dev/null`
      try {
         return sh.execSync(command, { input })
      }
      catch (error) {
         return [false, error.message]
      }
   }
}
